import React from 'react';
import { Suspense } from 'react';
import { APP_ROUTES } from './utils/app-routes';
import { useRoutes } from 'react-router-dom';

const App = () => {
  const appRoutes = useRoutes(APP_ROUTES);

  return (
    <>
      <Suspense>{appRoutes}</Suspense>
    </>
  );
};

export default App;
