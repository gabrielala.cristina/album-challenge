import { Input, Table } from 'antd';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { API_URL, LOCAL_STORAGE_KEYS } from '../constants';
const { Search } = Input;

const removeDuplicates = (arr) => {
  return arr.filter((item, index) => arr.indexOf(item) === index);
};

const Dashboard = () => {
  const [albums, setAlbums] = useState([]);
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);

  const getAlbums = async (userId = '') => {
    try {
      setIsLoading(true);
      const response = userId
        ? await fetch(`${API_URL}/albums?userId=${userId}`)
        : await fetch(`${API_URL}/albums`);
      const data = await response.json();
      setAlbums(data);
      setIsLoading(false);
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    getAlbums();
    localStorage.removeItem(LOCAL_STORAGE_KEYS.SELECTED_ALBUM);
  }, []);

  const columns = [
    {
      title: 'User ID',
      dataIndex: 'userId',
      key: 'id',
      filters: removeDuplicates(albums.map((album) => album.userId)).map((userId) => {
        return { text: userId, value: userId };
      }),
      onFilter: (value, record) => record.userId === value,
    },
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'id',
      render: (title, value) => (
        <a
          onClick={() => {
            navigate(`/${value.id}`);
            localStorage.setItem(LOCAL_STORAGE_KEYS.SELECTED_ALBUM, JSON.stringify(value));
          }}
        >
          {title}
        </a>
      ),
    },
  ];

  return (
    <div>
      <Search
        placeholder="User ID"
        onSearch={(value) => getAlbums(value)}
        style={{
          width: 200,
          marginBottom: 20,
        }}
      />
      <Table columns={columns} dataSource={albums} loading={isLoading} />
    </div>
  );
};

export default Dashboard;
