import React, { useState, useEffect } from 'react';
import { Col, Row, Spin } from 'antd';
import { API_URL, LOCAL_STORAGE_KEYS } from '../constants';

const Album = () => {
  const [photos, setPhotos] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const selectedAlbum = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEYS.SELECTED_ALBUM));

  const getPhotos = async () => {
    try {
      setIsLoading(true);
      const response = await fetch(`${API_URL}/photos?albumId=${selectedAlbum.id}`);
      const data = await response.json();
      setPhotos(data);
      setIsLoading(false);
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    getPhotos();
  }, []);

  return (
    <>
      {isLoading ? (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100%' }}>
          <Spin size="large" />
        </div>
      ) : (
        <Row gutter={[16, 16]}>
          {photos.map((photo) => (
            <Col key={photo.id}>
              <img src={photo.thumbnailUrl} />
            </Col>
          ))}
        </Row>
      )}
    </>
  );
};

export default Album;
