import React from 'react';
import Navigation from './navigation';
import SideNavigation from './side-navigation';
import { Layout } from 'antd';
const { Content } = Layout;

const LayoutWrapper = ({ children }) => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Navigation />
      <Layout>
        <SideNavigation />
        <Layout>
          <Content
            style={{
              padding: 20,
            }}
          >
            {children}
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};

export default LayoutWrapper;
