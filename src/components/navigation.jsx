import React from 'react';
import { Layout, Button } from 'antd';
const { Header } = Layout;

const Navigation = () => {
  return (
    <Header
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'right',
      }}
    >
      <Button type="primary" shape="circle" style={{ marginRight: 5 }}>
        CS
      </Button>
    </Header>
  );
};

export default Navigation;
