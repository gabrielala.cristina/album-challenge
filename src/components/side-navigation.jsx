import React, { useState } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { DashboardOutlined } from '@ant-design/icons';
const { Sider } = Layout;

const getItem = (label, key, icon) => {
  return {
    key,
    label,
    icon
  };
};

const items = [getItem('Dashboard', '/', <DashboardOutlined/>)];

const SideNavigation = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const [collapsed, setCollapsed] = useState(false);

  return (
    <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)}>
      <Menu
        onClick={({ key }) => {
          navigate(key);
        }}
        theme="dark"
        defaultSelectedKeys={[pathname]}
        mode="inline"
        items={items}
      />
    </Sider>
  );
};

export default SideNavigation;
