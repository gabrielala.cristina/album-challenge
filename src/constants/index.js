export const API_URL = 'https://jsonplaceholder.typicode.com';

export const LOCAL_STORAGE_KEYS = {
  SELECTED_ALBUM: 'selected-album',
};
