import LayoutWrapper from '../components/layout-wrapper';
import Dashboard from '../pages/dashboard';
import Album from '../pages/album';


export const APP_ROUTES = [
  {
    path: '/',
    element: (
      <LayoutWrapper>
        <Dashboard />
      </LayoutWrapper>
    ),
  },
  {
    path: ':id',
    element: (
      <LayoutWrapper>
        <Album />
      </LayoutWrapper>
    ),
  },
];
